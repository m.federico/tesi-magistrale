# (yarygina:overcoming_security) Overcoming Security Challenges in Microservice Architectures

È importante capire la diversità che i microservizi portano (del codice che prima era nascosto ora è raggiungibile, fiducia tra le varie parti). È importante che le difese siano scalabili, leggere, automatizzabili.

Fa un buon confronto con la SOA e con i monoliti. I problemi che affliggono i microservizi sono simili a quelli che affliggevano SOA. Il paragone che si fa spesso tra microservizi e monolite è sbagliato in parte, un monolite può essere estremamente modulare. I microservizi sono in pratica un insieme di modulare di sistemi distribuiti che comunicano attraverso la rete.
Ci sono problemi per quanto riguarda il testing, il debug, la gestione distribuita dei dati, i cambiamenti repentini e continui nella topologia della rete. **Cit dal tanenbaum**.

La sicurezza nei microservizi è un problema sfaccettato che dipende molto dalle tecnologie usate e dall'ambiente quindi per affrontarla occorre scomporla nei suoi componenti costituenti

Modello di attacco interessante. Assumiamo che un attaccante sia in grado di compromettere almeno un servizio all'interno della rete.

Principi generali:

1. Fai una cosa e falla bene
2. Deployment automatico ed immutabile
3. Isolamento e loose coupling
4. Eterogeneità delle tecnologie usate
5. Fail fast (i servizi non devono rimanere in uno stato mezzo mezzo che aiuta l'attaccante)

Alcune tecniche:

- autenticazione tra servizi per le comunicazioni tramite mTLS (ci sono due varianti, una di Docker ed una di Netflix)
- Autenticazione degli utenti tramite token. I token devono avere vita breve per evitare problemi. I vari nodi prendono decisioni sulla base di informazioni note localmente per validarli
- Uso dei certificati come metodo per l'autorizzazione oltre che per l'autenticazione. In pratica lo schema dei certificati e delle CA deve riflettere le relazioni tra i microservizi (una CA per microservizio), così nel momento in cui un servizio cerca di comunicare con un altro la comunicazione con mTLS viene autorizzata solo se esiste una CA che consente quella comunicazione.

# (sun:security_as_service) Security-as-a-Service for Microservices-Based Cloud Applications
L'idea di base consiste nell'usare le SDN per monitorare completamente tutto il traffico tra i servizi. In questo modo diventa possibile avere i sistemi di monitoring separati dai servizi ed allo stesso tempo mediare completamente le comunicazioni. Grossi problemi diventano l'efficienza e la flessibilità, inoltre introdurre le SDN significa introdurre un altro componente che deve essere monitorato. L'idea però di usare degli IDS e IPS per controllare le comunicazioni non è male per niente.

Viene fatta differenza tra "un monitor per nodo" ed "un monitor per servizio". Lui parla di nodi cloud. 

C'è il problema che gli IDS sono dei punti debolissimi così (*vabbè che fanno parte dell'infrastruttura alla fine*), filtrano il traffico e fanno tutto loro. L'approccio poi va bene in cloud ma non da altre parti. Infine fanno schifo le performance, loro stimano una velocità pari al 20% di quella iniziale a causa delle comunicazioni iper complesse, quindi va a minare la disponibilità.

Potrei usarlo come pratica da non seguire, magari.

# (yarygina:low_level_exploitation) Low-Level Exploitation Mitigation by Diverse Microservices
L'idea di base consiste nel far sì che anche se un attacco ha successo e garantisce il controllo di un servizio, di un container o di una VM, comunque i movimenti laterali sono resi difficili e macchinosi. In pratica se i servizi usano linguaggi e tecniche diverse un exploit fatto per un linguaggio non va bene per gli altri e quindi si bloccano i movimenti laterali.

Le API esposte devono essere minime e devono appartenere alla categoria di Chomsky più alta possibile. Non devono esserci percorsi inutili di comunicazione né scorciatoie in modo da ridurre la superficie di attacco complessiva. Valgono comunque le pratiche di segmentazione della rete, i nodi importanti vanno messi sotto chiave dietro a protezioni importanti. Si possono usare variazioni dei programmi introdotte dai compilatori.
Un serivizio compromesso si potrebbe usare anche come honeypot

# (carter:preliminary_methodology) A Preliminary Design-Phase Security Methodology for Cyber–Physical Systems
L'idea di base è quella di costruire un processo iterativo che prevede di valutare durante lo sviluppo i problemi di sicurezza. Servono tre team affiatati e coesi che sappiano scambiarsi informazioni.

Vedi parte sottolineata viola per tutti i dettagli

# (antonioli:gamifying_security) Gamifying ICS Security Training and Research: Design, Implementation, and Results of S3
Si possono usare dei giochi per insegnare agli addetti ai lavori a lavorare e per rendere più interessanti le lezioni. Si possono anche usare come allenamento 

# (jones:system_aware_security) System-Aware Cyber Security
Propone di costruire la sicurezza di sistemi in maniera consapevole, cioè di non inserire solo misure di sicurezza generali, ma anche di usare misure specifiche per un determinato ambiente e soprattutto la conoscenza che gli sviluppatori hanno di quell'ambiente. Questo potrebbe aiutare ad esempio ad intensificare i controlli in certi situazioni molto specifiche. In realtà questa parte è abbastanza generale dato che essenzialmente si tratta di tuning di un sistema alla fin fine

# (jones:system_aware_security_architecture) A System-Aware Cyber Security architecture

Propone di includere componenti di sicurezza all’interno del sistema stesso da proteggere, con componenti in grado di raccogliere dati dal sistema e prendere autonomamente misure di sicurezza.

I sistemi moderni sono sviluppati sempre più velocemente, questa cosa aiuta il time-to-market ma comporta difficoltà per quanto riguarda la sicurezza informatica.

Le soluzioni prposte devono tenere conto delle caratteristiche del sistema stesso, ovvero cosa fa, cosa contiene, quali componenti ci sono (non ha senso difendere qualcosa che non c’è o usare difese generali perché vengono vendute così).

> These features can be designed to deter attackers from exploiting a system; avoid attempts to compromise a system; identify when a system has been compromised, prevent the system from being damaged, isolate the compromised components, and enable restoration of the system to a noncompromised state; and enable operators and administrators to confirm that an event has been caused by a cyber attack and to take the appropriate action(s)

1. Diversità: usare funzionalità ridondanti ed evitando quindi monoculture (*va d’accordo con l’articolo low-level exploitation*). Funziona sia come deterrente sia come ausilio al recovery visto che se una funzione si blocca ci sono le altre. Allo stesso mtepo questo complica la manutenzione e la progettazione
2. Configuration hopping: obbliga l’attaccante ad agire avendo il tempo come avversario., però se l’attacco è facile e non richiede troppo tempo questa misura non è troppo efficace. La frequenza dei cambiamenti ed il tipo di cambiamenti dipende dal caso specifico e dal sistema stesso. Un altro problema riguarda il fatto che i cambi di configurazione non devono introdurre dei vuoti o dei momenti di mancanza di controllo
3. Data consistency
4. Analisi forensi tattiche: servono per distinguere attacchi da altre cause di malfunzionamento

Tutte queste misure consumano risorse e possono degradare le performance del sistema

# (leuprecht:castle_model) Beyond the Castle Model of cyber-risk and cyber-security

Storicamente si delinea un confine tra il dentro ed il fuori, il noi ed il loro. Questo dà un **falso senso di sicurezza**, mostra che si sta facendo qualcosa, ma in realtà fa poco per fornire veramente sicurezza. Nel momento in cui un attaccante è entrato all'interno del sistema se le difese sono solamente sulle mura diventa facilissimo muoversi all'interno.

Ci sono più domini di "dentro", il primo dominio è spaziale, il secondo è temporale, il terzo è relativo alla dicotomia online/offline.

Il nuovo mondo digitale sta corrodendo le mura dall'interno, mura più flessibili sono molto comode e costano meno. La tecnologia le sta corrodendo dall'esterno. Ci sono sempre nuovi attacchi, nuove tecniche, nuove connessioni. Anche il ricambio generazionale sta causando problemi. Le nuove generazioni hanno valori ed abitudini diverse rispetto alle vecchie, tendono a stare in ambienti più ampi e con meno restrizioni.

La soluzione è quindi quella di progettare il sistema per funzionare anche quando un attacco è in corso, il sistema deve rimanere protetto anche nel caso in cui c'è un attacco in corso. Possono essere utili ambienti virtuali (VM e WebApp). È importante inoltre ragionare, oltre che a livello di bit, a livello di intenti e effetti ad alto livello.

# (coelho:security) Security in Microservices Architectures
Grande riassunto iniziale delle caratteristiche dei microservizi (vedi parti sottolineate).  
Delinea poi i rischi che derivano dall'architettura. I microservizi di solito sono progettati per fidarsi l'uno dell'altro. L'autenticazione è importante, soprattutto per il fatto che i servizi sono sempre lì a comunicare l'uno con l'altro. Dato che usano tecnologie web (spesso) diventa importante proteggerli dagli attacchi tipici del web (XSS, CSRF, API non protette...)

# (hardy:confused_deputy) The Confused Deputy
Fa un grande esempio del concetto di "confused deputy".

> Il problema fondamentale era che il compilatore girava con il potere conferitogli da due autorità

La soluzione proposta nell'articolo consiste nel fare uso accorto di capability.

L'esempio che si estrae nel caso dei microservizi consiste in un client compromesso che effettua una richiesta ad un server non compromesso. Assumiamo che il server abbia la facoltà di accedere a dei dati riservati a cui il client non può accedere (come sempre accade nei microservizi). Una richiesta fatta ad arte può ingannare il server affinché compia qualcosa di stupido con il potere di cui è investito.

# (mczara:dependecies) Modeling and Analysis of Dependencies between Microservices in DevSecOps
Nel momento in cui si ha a che fare coi microservizi diventa importante non solo analizzare i singoli servizi ma anche le dipendenze, che hanno un impatto diretto nel momento in cui viene fatto recovery. Per questo motivo è importante avere chiare le dipendenze a monte delle operazioni di recovery. Inoltre conoscere le dipendenze e sfruttarle debitamente aiuta a snellire i tempi di recupero.

L'impatto che un malfunzionamento ha dipende direttamente sia dalle caratteristiche del malfunzionamento ma anche dal numero di microservizi coinvolti. Prendendo spunto dalle tecniche manageriali si cerca di raggiungere un certo livello di indipendenza tra le varie parti in modo che anche se qualcosa si rompe tutto il resto può continuare a funzionare (magari a funzionalità ridotte).

> Un'applicazione resiliente non è solamente quella che ha pochi errori, ma anche quella che è in grado di riprendersi velocemente dopo un malfunzionamento

**Figura 2 importante** mostra come dando priorità ai link più importanti il tempo complessivo di recupero cali rispetto al caso in cui i servizi vengono riattivati a caso.

Descrive poi il modello matematico, da non mettere nella tesi, magari giusto un accenno sul fatto che usa delle matrici. 

# (sun:automated_analysis) Automated Security Analysis for Microservice Architecture
Propongono di utilizzare uno strumento automatico per trovare vulnerabilità all'interno della progettazione. L'idea di base è quella di usare un linguaggio di descrizione formale per descrivere il comportamento dei componenti e poi di usare un motore logico e semantico per ricavare la possibilità di attacchi a partire dalle descrizioni.

Un'idea molto utile è quella di usare dei grafici (loro parlano di grafi a radar) per mostrare i risultati sia al personale tecnico (per sveltire le comunicazioni) sia al personale non tecnico (in modo che possa capire meglio).

# (pereira:security_mapping) Security Mechanisms Used in Microservices-Based Systems: A Systematic Mapping
Hanno fatto un grande elenco di ricerche correlata alla sicurezza nei microservizi. Una cosa interessante trovata consiste nel fatto che molti degli studi si concentrano sulla prevenzione e sulla mitigazione, ma pochi ci concentrano sul recovery. 

È stato un articolo utile più che altro per trovare altro materiale

# (cinque:advanced_monitoring) Advancing Monitoring in Microservices Systems
Propongono un framework di monitoraggio passivo, cercando di non essere intrusivi quanto più possibile. Usano log, statistiche dell'ambiente di esecuzione...

I problemi principali nel monitoring dei microservizi sono:
- Natura dinamica dei servizi
- Composizione di servizi molto diversi e che a volte devono funzionare come scatole nere
- Scarsa possibilità di intervento manuale
- Eterogeneità

Vogliono usare sniffing e analisi dei log

# (cinque:microservices_monitoring) Microservices Monitoring with Event Logs and Black Box Execution Tracing

Leggere i log è complicato sia per le macchine sia per gli umani dato che i log di servizi diversi possono avere formato e semantica diversi. Vogliono quindi accompagnarlo all'uso dello sniffing dei dati.

Si distingue tra monitoring diretto ed indiretto. Nell'articolo vogliono esplorare le potenzialità del monitoring indiretto dato che non prevede la collaborazione dei servizi coinvolti.

Ottengono i loro risultati senza aver bisogno di conoscere la struttura del sistema, ma guardando i tempi di risposta delle richieste ed i codici di completamento. In pratica analizzano le richieste e le risposte di HTTP controllandone l'header e controllando i parametri della concessione (come tempi di risposta, indirizzi, codici HTTP...).

Questo processo produce vari log che vengono analizzati con elasticsearch e kibana. Il sistema ideato serve anche semplicemente per poter leggere meglio i log e per indirizzare meglio le analisi fatte da un esperto umano, per non far perdere tempo insomma.

# (nehme:fine_grained_access_control) Fine-Grained Access Control for Microservices
Dà anzitutto una buona descrizione del problema del confused deputy e dei *powerful token*.

Propongono di usare dei token di sicurezza più piccoli oltre a quello generale che serve solo per dire "ehi, ho il permesso di interagire con il sistema". I token piccoli servono per autorizzare le richieste tra i vari servizi e hanno una breve durata per limitare i danni di furti ed abusi. Ogni microservizio si ritrova con un sidecar che si occupa di validare i token, così facendo le politiche di sicurezza sono separate dalle funzionalità dei singoli servizi.

Usano XACML per definire le regole generali di controllo dell'accesso che sono poi controllate dai singoli sidecar. In questo modo il token usato per richiedere un servizio non ha alcuna validità nelle richieste verso altri servizi e si mitiga il problema del confused deputy.

Per gestire bene i servizi potrebbe essere comodo trovare un compromesso, ovvero raggruppare i servizi all'interno di classi che raggruppano i servizi aventi simili caratteristiche di sicurezza. (Hanno fatto un prototipo).

# (jiang:intelligent_monitoring) Research on Intelligent Monitoring Scheme for Microservice Application Systems
Fa inizialmente una descrizione generale dell'architettura.

Si può monitorare a più livelli. A livello di  hardware, rete e sistema ci sono già soluzioni esistenti. Per quanto riguarda i microservizi invece ci sono più opzioni:
- Log: guarda i log per capire cosa sta succedendo. Propongono di usare elastic e kibana per cercare info nei log
- Tracing: controlla il flusso delle richieste e delle chiamate
- Misurazioni: controlla le misurazioni fatte nell'ambiente per cercare usi di risorse anomali. Non guarda tanto agli eventi quanto all'andamento degli stessi

Il grosso problema è il rumore nelle misurazioni, che può far scatenare allarmi quando non ve ne sono. In generale il componente che lancia l'allarme dovrebbe considerare i dati provenienti da varie fonti allo stesso tempo

# (liu:unsupervised_detection) Unsupervised Detection of Microservice Trace Anomalies through Service-Level Deep Bayesian Networks
Propongono di collegare tutte le chiamate fatte da un servizio per mezzo di UUID. Tutte le chiamate con lo stesso UUID costituiscono la traccia di un certo servizio. Una traccia è anomala se devia dal comportamento normale appreso in precedenza, tuttavia hanno dimostrato come questo semplice approccio ha difficoltà nel collegare i tempi di risposta alle situazioni anomale. Quindi propongono una rete bayesiana unica per tuttala rete in grado di determinare queste situazioni. Vogliono unificare queste due misurazioni in quanto una interazione tra servizi potrebbe essere normalissima eppure avere un tempo di risposta diverso rispetto ad altre osservazioni (questo dipende da quali dati deve estrarre o elaborare, dal carico della macchina su cui gira il processo dei servizi eccetera). Rimane un problema aperto l'interpretabilità da parte di un essere umano.

Devono usare un algoritmo non supervisionato perché ci sono troppi dati da analizzare

# (atighetchi:requirement_analysis) Security Requirements Analysis – A Vision for an Automated Toolchain
*Ha come problema il fatto che dice quello che vogliono fare, ma non lo fanno. Fornisce comunque buoni spunti*

Al momento si procede all'uso di CVA per trovare vulnerabilità, ma trovarle una volta che sono già state implementate in realtà costa molto di più che trovarle quando il sistema è stato progettato.

Propongono un sistema in grado di estrarre informazioni dai requisiti espressi in forma testuale, in modo da trovare prima possibile i problemi.  
Prevede di usare un estrattore in grado di processare il linguaggio naturale al fine di trovare ed analizzare i requisiti funzionali e di sicurezza. Alla fine viene generato un rapporto che include l'esito delle varie operazioni.
Propone anche di effettuare la "gap analysis" per trovare requisiti mancanti (come logging...).

# (gomi:continuous_authentication) Continuous Authentication System Using Online Activities
Qualsiasi sistema moderno può collezionare informazioni aggiuntive sugli utenti, come la posizione e l'orario dell'utilizzo, la frequenza e l'uso dei tasti sulla tastiera, le preferenze (vedi sito di e-commerce), la semantica delle ricerche e delle richieste. Tutte queste informazioni possono essere usate per autenticare un utente, creando il concetto di ABA (activity based authentication).

Per identificare ed autenticare gli utenti usano un insieme di dati correlati, dato che una singola azione non è in grado di autenticare un utente. C'è però il problema della validità temporale delle azioni, quelle troppo vecchie potrebbero non essere più valide. Una volta raccolti i dati usano il machine learning per creare un profilo di un utente e la effettuare la classificazione (multiclasse). 

Il problema rimane comunque per l'interpretabilità dei risultati e per le performance. Ci sono problemi per quanto riguarda il mantenimento del dataset, visto che continua sempre a crescere e che i dati vecchi diventano man mano inutili. All'aumentare del numero di utenti poi diminuisce la precisione dell'autenticazione, propongono di effettuare un'autenticazione su più livelli in modo da raggruppare gli utenti via via in gruppi più piccoli. C'è anche il problema del cold start, ovvero quando un utente è nuovo non ci sono dati utili ad autenticarlo.

# (gellman:nsa_google) NSA infiltrates links to Yahoo, Google data centers worldwide, Snowden documents say
L'NSA ha spiato dentro le reti private di google e yahoo

# (ietf:rfc4347) Datagram Transport Layer Security
Funziona tutto come TLS, ma permette che i messaggi possano arrivare in ordine sparso. Questo comporta il fatto che il cifrario da utilizzare non sia a flusso, ma a blocchi dato che i pacchetti potrebbero non arrivare in ordine.

Può essere attaccato creando tante richieste di handshake per il server oppure inviando richieste di handshake con l'origine modificata in modo che il server poi proceda con i passi seguenti (il primo di solito è l'invio di un certificato che può essere abbastanza grande) per ognuno degli host che paiono aver inviato la richiesta.

# (saravi:cost) Estimating Cost at the Conceptual Design Stage to Optimize Design in terms of Performance and Cost

Stimano che fino all'80% dei costi sono decisi in fase di progettazione. Prima le decisioni vengono prese minore è l'impatto che hanno sui costi.

# (jin:anomaly_detection) An Anomaly Detection Algorithm for Microservice Architecture Based on Robust Principal Component Analysis
Dato che è un'architettura distribuita con molti pezzettini diventa difficile trovare quale sia il componente preciso ad aver creato problemi, inoltre è sempre possibile che si verifichi un effetto valanga (un errore in un servizio provoca un errore in un altro e così via).

Vogliono rilevare le anomalie solo sulla base delle anomalie nell'ordine e nelle caratteristiche delle chiamate. Usano una forma modificata della PCA fatta per lavorare con grandi moli di dati. Lo fanno per ricavare le informazioni importanti su cui far allenare gli algoritmi.

Indicatori importanti di anomalie sono l'utilizzo della CPU, memoria consumata, ritardo nelle risposte.
*In realtà indicatori come l'utilizzo della CPU e la memoria consumata sono poco indicativo nel caso dei microservizi, dato che il numero di istanze e la collocazione delle istanze tipicamente può variare nel tempo seguendo picchi di carico anche giornalieri*

# (samir:anomaly_detection) DLA: Detecting and Localizing Anomalies in Containerized Microservice Architectures Using Markov Models
Nel mondo a microservizi ci sono alcune caratteristiche che complicano il monitoring delle performance, in particolare la flessibilità del deployment. Se ad esempio il calo di performance non è localizzato presso la causa può darsi che la causa non venga mai trovata in realtà.

Raccolgono vari indicatori di performance e all'interno di questi cercano quali siano i KPI per un certo sistema. Una volta trovati li usano per rilevare anomalie. Per essere ragionevolmente certi che l'anomalia sia reale e non una semplice fluttuazione usano degli indici di correlazione.

Per raccogliere i dati usano sonde installate sulle singole VM ed usano i tempi di risposta

# (sassman:halting) The Halting Problems of Network Stack Insecurity
Propongono la tesi secondo cui la semplicità di interpretazione dei dati di input di un qualsiasi programma incrementi la sicurezza diminuendo la possibilità di comportamenti non definiti. Nel caso di input troppo complessi l'analisi diventa un problema indecidibile.

Parser automatici sono stati usati dagli anni 60 per la costruzione di compilatori e riconoscitori per linguaggi, tuttavia non sono mai stati usati in maniera estensiva per la generazione ed il riconoscimento di protocolli. Sono utili anche perché le differenze tra due implmentazioni diverse dello stesso protocollo tipicamente possono portare a dei bug, in quanto portano ad avere input considerati in maniera diversa tra versioni diverse. Differenze che possono essere sfruttate

> Decidability matters. Formally speaking, a correct protocol implementation is defined by the decision problem of whether the byte string received by the stack’s input handling units is a member of the protocol’s language. This problem has two components: first, whether the input is syntactically valid according to the grammar that specifies the protocol, and second, whether the input, once recognized, generates a valid state transition in the state machine that represents the logic of the protocol. The first component corresponds to the parser and the second to the remainder of the implementation.

Propongono alcuni pricncipi:
1. principio delpotere computazionale minimo
2. sicurezza della composizione software

# (yarygina:restful_not_secure) RESTful Is Not Secure

REST non è compatibile con alcuni vincoli che la sicurezza impone. Non è fatto per essere sicuro, ma per soddisfare i bisogni dell’internet dei primi anni 2000. Alcuni dei 6 principi (client-server, uniform interface, layered system) sono abbastanza di alto livello da supportare l sicurezza e da non intralciarla, altri (stateless, cacheable, code-on-demand) la ostacolano direttamente:

* Stateless: il grosso problema è che non esistono rptocolli di sicurezza stateless, non si possono prevenire certi attacchi senza mantenere uno stato minimo su almeno una delle due parti
* Cache: non tutti i dati possono essere slavati incache, inoltre per salvare dati in cache devono essee in chiaro, quindi tutte le risposte dovrebbero transitare non protette
* Code-on-demand: l’esecuzione di codice arbitrario è tremenda

Di base ci sono due meccanismi di autenticazione usati nelle API RESTful e REST-like:	

* Token: uso un token come sostituto delle credenziali per autenticarmi
* Client side request signing: il client firma la propria richiesta (o magari un suo hash per comodità)

# (topalovich:short_certificate) Towards short-lived certificates

OCSP è un protocollo praticamente morto, tanto che Google lo sta disabilitando e sta incominciando a distribuire le CRL tramite gli aggiornamenti di Chrome stesso. Il problema è che questo toglie potere dalle mani delle CA, a cui dovrebbe appartenere, per creare un nuovo collo di bottiglia che va bene per il lato desktop ma non per il mondo mobile.

Propongono di usare certificati con la durata di qualche giorno (quattro). Quando un sito riceve un certificato valido per un anno in realtà riceve tanti piccoli certificati di breve durata. Propongono di inserire un nuovo tag per i certiifcati che dica al client che quello è un certificato di breve durata e quindi di bloccare ogni richiesta che venga fatta dopo la scadenza.

Aiuta a prevenire degli attacchi:

* L’attaccante può anche compromettere una CA, tuttavia visto che i certificati hanno durata breve la compromissione deve essere reiterata nel tempo
* L’attaccante potrebbe rubare la chiave privata del server, ma visto che icertificati hanno vita breve questo ha effetti limitati nel tempo
* L’attaccante potrebbe fare DoS alla CA e quindi impedire di recuperare i certificati ai client, ma questo significa che l’attaccante dovrebbe essere in grado di bloccare la CA per almeno qualche giorni senza interruzioni e senza essere notato

Questa tecnica non richiede modifiche ai client (*cosa buona visto che così possiamo inserire questa cosa come plugin nelle architetture già esistenti*).

# (microsoft:api_gateway) The API gateway pattern versus the direct client-to-microservice communication

Disaccoccoppia i client dall’implementazione, riduce il numero di RTT tra client e servizi

È una buona idea usare diversi gateway, uno per ogni tipo di client, in modo da lasciare l’implementazione pulita

Può diventare un collo di bottiglia e crea un single-point-of-failure

# (richardson:patterns) Microservices Patterns

* CAP3
  * Parla dei vari meccanismi di IPC, oggi va di moda REST ma ce ne sono altri. Fa lo schema delle tipologie di comunicazione
  * Per i cambiamenti minori le API devono rimanere retrocompatibili, per quelli maggiori o si inserisce il numero di versione all’interno dell’URL (per REST) oppure si usa un MIMEtype che consente di scegliere
  * Ci sono tante tecnlogie tra cui scegliere (*ci concentraimo su REST*), ma usiamo REST. REST ha un IDL che deriva da swagger. Per usare REST serve per forza un meccanismo di service discovery
    * Pattern self registration: un servizio registra se stesso. Eventualmente il DS può usare degli heartbeat per cercare di capire se i servizi sono ancora lì
    * Pattern 3rd party registration: un qualcosa che solitamente fa parte della piattaforma di deployment si occupa di registrare in automatico il servizio che ha appena tirato su
    * Pattern client side discovery
    * Pattern server side discovery
  * Ci sono anche tutti gli altri schemi da inserire eventualmente

* CAP 8
  * I vari client hanno necessità diverse, problemi diversi. Un PC desktop tipicamente può usare più dati visto che spesso è collegato via cavo ad una LAN, un cellulare tipicamente usa dati mobili e quindi ha meno banda disponibile. Quindi è una buona idea avere API diverse per client diversi (portato all’estremo questo è il pattern frontend for backend)
  * Dover interagire con i singoli servizi direttamente è anche una fonte di potenziali errori perché lato client le richieste devono essere molto più complesse. Inoltre rivela la struttura interna del sistema. Avere tutte le chiamate dirette poi fa sì che l’evoluzione del sistema sia più lenta, dato che gli aggiornamenti interni vanno a riflettersi direttamente sui client che quindi devono essere aggiornati spesso (se lo fanno e quando lo fanno)
  * API gateway può fornire anche funzioni per gli utenti come autenticazione, autorizzazione, limitazione del numero di richieste, caching, raccolta di dati statistici, logging. Queste funzioni potrebbero essere implementate dai singoli servizi (male), da qualcosa messo prima del API gateway (fatto che migliora la sicurezza fornendo un unico punto di accesso, utile soprattutto se vi sono vari gateway), oppure sul gateway stesso
  * Il problema col gateway è che le responsabilità sono confuse. Chi è responsabile di cosa? Di chi è la colpa quando succede qualcosa? Chi deve intervenire?

# (mavroeidis:threat_intelligence) Cyber Threat Intelligence Model: An Evaluation of Taxonomies, Sharing Standards, and Ontologies within Cyber Threat Intelligence

La threat intelligence è la generazione di conoscenza basata su dati fattuali riguardo a minacce esistenti o potenziali. Questo porta vari benefici come una migliore efficienza ed una maggiore efficacia dei processi di difesa.

> Threat intelligence is referred to as the task of gathering evidence-based knowledge, including context, mechanisms, indicators, implications and actionable advice, about an existing or emerging menace or hazard to assets that can be used to inform decisions regarding the subject’s response to that menace or hazard

Propongono di usare la IA (che originalità….). Propongono due modelli che si sovrappongono per valutare la situazione.

Dicono che serve un qualcosa di unificato che consenta di comunicare e condividere la conoscenza tra i vari esperti in maniera efficiente, così da rendere tutte le parti coinvolte nel processo consapevoli anche di cose che non hanno mai visto

> Knowledge collection: Much of the knowledge used by most of skilled analysts today is residing only in their heads. If we manage to model this knowledge and express it in an ontology, not only more analysts would be able to consume this type of intelligence, but the analytics would be executed in a consistent way, contradicting the ”confirmation bias” often referred to in an investigation

L’attribuzione di un attacco ad un attore è importantissima, ma gli attori esperti usano teniche per rendere difficile questa pratica.

Giungono a delle conclusioni generali (*che posso usare nella tesi*)

> First, formal terminology (definitions) and vocabularies should be described. Second, all the abstraction layers of the cyber threat intelligence model should be included and expressed properly in the ontology. Third, knowledge coming from domain expertise in a structured way should be gathered and formally represented in the ontology to facilitate advanced reasoning based on relationships between data. Fourth, constraints should be defined and constructs should be used in the ontology enabling the reasoning capabilities lying within the OWL language. Finally, the use of subjective logic to model trust in sources and confidence in information

# (osman:threat_intelligence_microservices) Sandnet: Towards High Quality of Deception in Container-Based Microservice Architectures

L’uso di sistemi di sicurezza può ridurre gli effetti dannosi di un attacco, ma allo stesso tempo riduce anche le informazioni che possono essere raccolte tramite threat intelligence

Alcuni KPI possono essere usati anche dagli attaccanti per determinare la presenza di honeypot.

Propongono di usare le SDN per confinare i microservizi attaccati e compromessi ed allo stesso tempo di usare dei checkpoint per ripristinare il servizio nella rete in cui dovrebbe stare ad uno stato valido. Il gruppo di servizi spostati cresce e muta man mano che l’attaccante si sposta. Se l’attaccante scopre questa cosa può eseguire tanti attacchi in contemporanea creando un attacco DDoS

Modello dell’avversario:

> It is capable of scanning the network, conducting reconnaissance, performing traffic analysis to fingerprint or counter the network sandbox, actively crafting malicious packets to forged network destinations, and connecting to containers within its reach as well as leverage remote exploits in them. The adversary is stealthy and aims to remain undetected. Hence, it behaves like the MS it attacks which allows it to evade internal firewalls by masquerading as a benign container

Per far funzionare il tutto al meglio le procedure non devono modificare troppo i vari KPI del sistema in modo da non farsi sgamare dall’attaccante. In realtà questo procedimento ha un costo in termini di risorse e di tempo, più è lungo più tempo ha l’attaccante di espandersi.

# (bertero:log_nlp) Experience Report: Log Mining using Natural Language Processing and Application to Anomaly Detection
L'analisi dei log non scala. Ogni servizio ha le proprie keyword, le proprie convenzioni, quindi trovare in automatico un problema all'interno di un file di log non è semplice. Propongono di usare tecniche di NLP per risolvere il problema. L'idea di base è quella di usare un algoritmo di NLP per mappare le parole e le farsi all'interno di uno spazio n-dimensionale e poi di usare tecniche di clustering per determinare lo stato del sistema a partire da quello schema specifico.

# () Revisiting Architectural Tactics for Security
La sicurezza deve essere realizzata a livello di sistema, non del singolo applicativo. Se ci sono più applicazioni devo condividere gli stessi meccanismi.

# (keim:cti) Cyber threat intelligence framework using advanced malware forensics
Distinzione tra IT strategica, tattica ed operativa
Le sorgenti dei dati possono essere sia interne che esterne

Propongono un framework a tre livelli a mo' di  pipeline:
* lv 1: input di dati
* lv 2: analisi dei dati
* lv 3: gestione delle minacce, ricerca delle soluzioni

# (granchelli:recovery) MicroART: A Software Architecture Recovery Tool for Maintaining Microservice-based Systems
Ci sono pochi studi sulle operazioni di recovery nell'ambito dei microservizi

# (alshuqayran:recovery) Towards Micro Service Architecture Recovery: An Empirical Study
Mancano riferimenti per le procedure di recovery

Procedura per il recovery a due fasi, nella prima si crea il piano di recovery, nella seconda si valida il piano con dei test per vedere se funziona

# (pardon:bac_theo) Consistent Disaster Recovery for Microservices: the BAC Theorem
> When Backing up an entire microservices architecture, it is not possible to have both Availability and Consistency

Se si prendono backup indipendenti dei microservizi in momenti diversi questi non sono coerenti dato che alcuni backup referenziano dati assenti negli altri però si mantiene la disponibilità del sistema. Se si vuole mantenere la coerenza dei dati bisogna bloccare tutto il sistema, facendo mancare la disponibilità ed introducendo un forte accoppiamento tra due servizi.
Bisogna trovare il modo di risolvere le condizioni di incoerenza dei dati oppure fare in modo che non avvengano proprio tramite la progettazione dell'architettura oppure semplicemente essere consapevoli del fatto che possono esistere.
