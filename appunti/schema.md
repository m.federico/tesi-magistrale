---
title: "Schema tesi"
author: "Federico Marchetti"
---

[TOC]



# 1. Introduzione

Materiale:
- nginx:design
- sun:security_as_service
- coelho:security
- pereira:security_mapping
- richardson:patterns

Note:  
- Obiettivi
  - fornire una formalizzazione dell’architettura dal punto di vista della sicurezza a 360°
  - fornire un framework per la progettazione di un sistema sicuro
  - fornire un riferimento per il mantenimento della sicurezza e per la reazione ad eventuali minacce

# 2. Architettura

Materiale:

- nginx:design
- yarygina:overcoming_security
- sun:security_as_service
- coelho:security
- gellman:nsa_google
- leuprecht:castle_model
- galanti:tesi
- richardson:patterns
- newman:principles_microservices

Note:

## 2.1. Introduzione all'architettura
- Descrizione architettura
- Componenti principali
- Confronto con architettura monolitica
- Diagramma 6 livelli di Yarygina
- Vantaggi e svantaggi
  - Uso con agile e devops (nginx:design)
- Microservizi e container/VM/cloud
## 2.2. Difendere i microservizi

Materiale:

* coelho:security
* gellman:nsa_google
* galanti:tesi
* ibm:tls
* leuprecht:castle_model
* nginx:reference
* diogenes:sybersecurity_attack_defense

Note:

- Red team blue team
- Difesa in profondità (gellman:nsa_google, galanti:tesi, leuprecht:castle_model)
- Definizione perimetro di sicurezza
- Problematiche uniche dei microservizi
- Generale kill chain (diogenes:sybersecurity_attack_defense) + fase recon
- Affronto le varie componenti in ordine cercando di definire quali sono i problemi e quali le soluzioni (nginx:reference)
- L'idea di base è quella di non fidarsi di nessuno (z3r0 7rus7)

### 2.2.1. Servizi
Materiale:

- yarygina:low_level_exploitation

Note:

- Usare linguaggi diversi
- Usare estensioni del compilatore per rendere istanze dello stesso servizio leggermente diverse l'una dall'altra. Questo ad esempio aiuta a risolvere problemi nel momento del rideployment solo che un servizio è stato attaccato
- Eliminare percorsi di comunicazione inutili. Controllare se il grafo delle comunicazioni è denso


### 2.2.2. Comunicazioni
Materiale:

- yarygina:overcoming_security
- hardy:confused_deputy
- nehme:fine_grained_access_control
- gomi:continuous_authentication
- ietf:rfc4347
- ietf:rfc8446
- sassman:halting
- galanti:tesi
- suomalainen:tesi
- yarygina:restful_not_secure
- topalovich:short_certificate
- richardson:patterns
- ibm:tls
- osman:threat_intelligence_microservices 
- wikipedia:postel_law
- TODO SDN

Note:

- Ci sono più tipi di comunicazioni:
  - 1-1, 1-N, sincrone o asincrone (mostrare tabella)
- Digressione su REST (yarygina:restful_not_secure)
- Serve sicurezza a tutti i livelli (gellman:nsa_google). Serve garantire autenticità, riservatezza ed integrità (uso di mTLS). Bisogna tener conto che non esiste solo TLS, anche DTLS può avere la sua utilità.
  - Serve una PKI interna (evitando di approfondire, c’è già molto materiale)
  - Riservatezza è facile, TLS è cifrato
  - Integrità è facile, uso dei digest
  - Autenticazione si basa sui certificati (topalovich:short_certificate)
- Autorizzazione:
  - Tecniche:
    - Token (suomalainen:tesi, yarygina:overcoming_security)
      - JWT, Macaroon. Brevissima descrizione
    - Certificati (yarygina:overcoming_security)
      - Usare un sistema di certificati brevi (yarygina:overcoming_security)
      - Revoca e durata dfell’autenticazione
      - Richiesta di certificati
  - Schemi di controllo (suomalainen:tesi):
    - RBAC
    - MAC, DAC
    - ABAC
  - Problema della propagazione dell’autorizzazione (suomalainen:tesi)
  - Confused deputy (nehme:fine_grained_access_control, hardy:confused_deputy)
- API:
  - Per le API c'è il problema dell'evoluzione. Nel momento in cui sono supportate versioni diverse della stessa API per un attaccante diventa possibile richiedere di utilizzare la vecchia versione (in questo caso anche i sistemi di monitoring potrebbero diventare inutili se questa cosa viene fatta nel momento di transizione tra le due, dato che diventa difficile capire quando è lecito usare una o l'altra). È un attacco tipico, si poteva anche con TLS tipo. In ogni caso tutte le volte che c'è la possibilità di scegliere qualcosa si lascia spazio di manovra ad un attaccante
  - Lo stesso problema si presenta quando client e server supportano due API diverse ma compatibili, è vero che si possono ignorare i dati in più e che si possono usare dei default, ma di messaggi fatti ad arte ce ne sono tanti e tocca stare attenti (internet è stato progettato per essere interoperabile cit. sassman:halting)
  - Come discorso generale sulle API si può inserire quello sulle grammatiche di chomsky. Le API devono appartenere alla grammatica di livello più alto possibile (sassman:halting) e devono essere restrittive sugli input, anche se tradizionalmente in internet si è fatto il contrario (wikipedia:postel_law)

### 2.2.3. API gateway

Materiale:

* microsoft:api_gateway
* richardson:patterns

Note:

Nel momento in cui si ha un monolite si può fare un'unica richiesta (magari mediata da un proxy), ma comunque il monolite si occupa di fare tutto. Con i microservizi non si può fare una chiamata per servizio ed aspettarsi che sia il client ad aggregare le risposte. (C'è un'ottima descrizione nel liro di nginx). Si può fare l'esempio delle feature fornite da ["amazon API gateway"](https://docs.aws.amazon.com/apigateway/latest/developerguide/welcome.html) (e sotto pagine)

- Nasconde la struttura dei servizi quindi diventa più difficile coordinare un attacco (fase di recon)
- Deve essere in grado di comunicare con il DS in modo da poter indirizzare correttamente le richieste
- È un componente di frontiera che deve essere altamente disponibile ed allo stesso tempo molto robusto. Gli attacchi possono arrivare sia da dentro che da fuori, quindi non si può fidare di nessuno e deve essere protetto da entrambi i lati. Si devono mettere in sicurezza le comunicazioni, si deve fare in modo che non vada giù (uso di circuit breaker e timeout)
- Può essere un collo di bottliglia
- Fornisce
  - Autenticazione degli utenti (ma potrebbe anche fornirla un altro servizio)
  - Controllo degli accessi, rate limiting, API monitoring
  - Load balancing
  - Gestione della cache per ridurre la latenza delle risposte
  - Fornisce log delle connessioni in entrata

### 2.2.4. Service discovery

Materiale:

* richardson:patterns

Note: 

Si usa per scoprire dove sono le istanze dei servizi. Ci sono di base due pattern: client-side e server-side discovery (cambia solo chi fa la richiesta). Alla fin fine è un DNS sotto steroidi, quindi si ripresentano molti dei problemi che si hanno coi DNS

- Le comunicazioni devono essere autenticate cifrate ed integre, altrimenti sarebbe possibile per un attaccante registrare dei servizi fasulli, riempire la capacità di memorizzazione del servizio, cambiare la registrazione dei servizi già collegati
- L'insieme delle associazioni deve essere protetto, se è su un file esterno tipo DB devono essere fatti controlli di integrità
- Se questo servizio è replicato bisogno prevedere un qualche meccanismo in grado di sincronizzare le copie. La sincronizzazione però deve essere autenticata, sennò si ripresenta il problema della sincronizzazione dei DNS. Per i DNS è stato proposto DNSSEC
- È comunque un'arma per isolare i servizi infetti
- TLS non è l'unico protocollo disponibile. Nel caso vengano usati dei sidecar o la server-side discovery è possibile usare anche DTLS, visto che quello che serve in realtà è un datagramma

### 2.2.5. Gestione dei dati
Il concetto principale è che ci sono tanti DB e non uno solo per tutti. Questo viene fatto principalmente per evitare che i servizi siano troppo accoppiati l'un l'altro.

- Avere DB separati e schemi diversi fa sì che chi attacca debba compromettere vari sistemi e che la compromissione di un sistema da accesso a meno dati nel complesso. Lo svantaggio è che mancano le garanzie di ACID, quindi per un attaccante è più semplice bloccare le comunicazioni che servono a garantire la coerenza dei dati
- Per ottenere coerenza senza ACID si possono usare più tecniche:
  - Event-driven architecture: si usa pub sub per pubblicare gli aggiornamenti su un broker. A questo punto però il broker deve essere iper protetto, dato che praticamente contiene l'intero parco dati di tutti il sistema
  - Mining dei log: bisogna capire dove sono i log, e di nuovo vanno protetti fino alla morte per evitare che qualcuno ricavi i dati
  - Event store: si pubblicano gli aggiornamenti su un event store. I vari servizi si iscrivono per ricevere gli aggiornamenti, ma di nuovo tocca proteggerlo coi denti 
- Bisogna proteggere nel caso si pub-sub le comunicazioni con mTLS
- Attacchi: 
  - inondare di connessioni qualsiasi server/broker sia. Anche se la connessione è rifiutata si sprecano risorse di calcolo

### 2.2.6. Pattern di comunicazione

Materiale:
- suomalainen:tesi
- nginx:design

Note:
  - Breve accenno 

# 3. Progettazione di un sistema sicuro
Materiale:

- leuprecht:castle_model
- carter:preliminary_methodology
- jones:system_aware_security
- jones:system_aware_security_architecture
- antonioli:gamifying_security
- hardy:confused_deputy (in fase di progettazione tocca tenere conto delle capability)
- sun:automated_analysis
- atighetchi:requirement_analysis
- saravi:cost
- galanti:tesi
- crabtree:gamified_operations
- mczara:dependecies (il sistema va progettato per essere resiliente anche in caso di problemi)

Note:

- Framework (guarda email) (carter:preliminary_methodology)
  
  - Si integra anche con DevSecOps (ibm:devsecops), nello schema c’è una fase di pianificazine e il framework si inserisce lì. Il framework ha “memoria” di quanto è stato fatto nelle fasi precedenti e quindi permette di incrementare la sicurezza del sistema
    
  - Riguarda la difesa di architetture a microservizi, si concentra sull'organizzazione tra microservizi e sulla comunicazione tra questi. Non prende in considerazione la sicurezza dei singoli applicativi, il livello hardware, né la parte architetturale che ospita i servizi
  - Va utilizzato durante le prime fasi di progettazione del sistema o quando diventa necessario apportarvi modifiche. È così sia per motivi economici che di praticità. Prima i problemi relativi alla sicurezza vengono affrontati e risolti minore è l'impatto complessivo sui costi e minore è la necessità di apportare modifiche sostanziali in un secondo momento (saravi:cost)
  - Presenza di tre diversi team:
    - Yellow team con il compito di progettare il sistema e di mettere in pratica la metodologia aggiornando i vari documenti necessari
    - Blue team: con il compito di studiare la descrizione del sistema, valutarne la robustezza e di individuare soluzioni ai problemi di sicurezza individuati durante la fase di analisi effettuata dal red team o tramite strumenti automatici
    - Red team: con il compito di identificare e sfruttare le vulnerabilità al fine di mettere alla prova il sistema nel complesso.
  
    È importante che le tre squadre collaborino e che ci sia uno scambio di informazioni fitto/costante in modo da poter trovare e applicare soluzioni nel minor tempo possibile.
  - Varie fasi
  
    1. Descrizione iniziale del sistema
    2. Analisi della soluzione proposta da parte del blue team
    3. Successivamente vi è una fase di analisi delle priorità per valutare in quale ordine ed in quali termini affrontare (alcuni o tutti) i problemi trovati. 
    4. Analisi da parte del red team
    5. Tramite collaborazione dei tre team, vengono elaborati i risultati ottenuti al fine di trovare eventuali possibili soluzioni nel rispetto dei vincoli dettati dall'organizzazione e delle priorità prima trovate. 
    6. Se la soluzione trovata non è adeguata la descrizione del sistema e la lista delle priorità per riflettere i problemi risolti ed i nuovi problemi rilevati. Se la soluzione è adeguata diventa necessario valutarla anche dal punto di vista dell'organizzazione, ovvero valutare se i costi e le priorità sono stati rispettati
  
    Per le fasi di analisi dei requisiti ci si potrebbe aiutare anche con strumenti automatici (atighetchi:requirement_analysis). Alla fine della procedura si ottengono due risultati:
    - Una descrizione sicura del sistema
    - Un insieme di documenti che valutino la sicurezza dello stesso. Non è detto che debbano essere documenti puramente tecnici, possono anche essere documenti più "grafici" comprensibili anche al personale non tecnico


  - Principi generali
    - (yarygina:overcoming_security)
    - (yarygina:low_level_exploitation) la possibilità di utilizzare tecnologie e linguaggi diversi per implementare le varie parti dell'architettura, in modo da mitigare la possibilità di riapplicare un attacco ad un diverso servizio
    - la necessità di non fermare le difese al solo perimetro con l'esterno (leuprecht:castle_model)
    - la possibilità di rendere il sistema "elastico" in modo che possa reagire e tollerare eventuali intrusioni (in realtà devo ancora cercare altri articoli riguardo a questo argomento per approfondire ed espandere, visto che l'articolo in sé propone solamente un'idea)
- Identificare l'ordine con cui i vari componenti devono essere difesi e riabilitati al lavoro. È una cosa molto aziendale, si deve tenere in considerazione costi e benefici di ogni singolo componente. Si hanno due tipi di obiettivi:
  1. Strutturali: non sono importanti in sè, ma sono importanti in quanto servono come base per far funzionare altri servizi e per raggiungere altri obiettivi
  2. "Strategici": sono obiettivi importanti dal punto di vista "aziendale". Possono anche avere caratteristiche strutturali, ma rappresentano un obiettivo da difendere
  
  In pratica occorre individuare prima gli obiettivi strategici (con linguaggio naturale, in forma discorsiva). Da qui si ricavano quali effettivamente sono i servizi strategici. Una volta ottenuta la lista degli obiettivi da difendere si può passare ad identificare quali siano le dipendenze da difendere (gli obiettivi strutturali)
- Identificare quali sono le priorità del sistema. Non tutte le cose hanno lo stesso livello di importanza

## 3.1. Motivazioni

## 3.2. Progettazione

## 3.3. Identificazione delle priorità

# 4. Mantenimento della sicurezza

Materiale:

* galanti:tesi
* nist:recovery
* sun:security_as_service
* diogenes:sybersecurity_attack_defense

Note:

Non deve essere una ricetta precisa ma più un framework modulare in cui vari pezzi si incastrano per formare qualcosa di più complesso. Ognuno di questi punti rappresenta un diverso blocco che può anche interagire con gli altri.

## 4.1.  Piano di recovery

Materiale:

* nist:recovery

Note:

* Aggiornamento del piano di recovery in modo da essere sempre pronti a tutto

## 4.2. Allenamento del personale

Materiale:

* crabtree:gamified_operations
* antonioli:gamifying_security
* galanti:tesi

Note:

* Il personale deve essere allenato con 
  * giochi
  * esercitazioni

## 4.3. Monitoring
Materiale:
- jiang:intelligent_monitoring
- cinque:advanced_monitoring
- cinque:microservices_monitoring
- liu:unsupervised_detection
- jin:anomaly_detection
- samir:anomaly_detection
- jones:system_aware_security
- pfleeger:security_base
- osman:threat_intelligence_microservices

Note: 

- Ci sono varie tecniche per monitorare cosa succede, principalmente:
  - risorse usate (CPU, RAM, connessioni)
    - Alcuni KPI possono essere usati anche dagli attaccanti (osman:threat_intelligence_microservices)
  - comportamento dei microservizi (con quali altri microservizi interagiscono, quando, per quanto tempo)
  - latenza nelle risposte (una fluttuazione nel tempo di risposta significativa può indicare attività diverse da quelle che dovrebbe svolgere normalmente)
  - log. I log sono un'arma a doppio taglio, e va valutato l'uso in singola sede, da un lato dicono cosa sta succedendo per file e per segno con tanto di orario, dall'altro sono un altro obiettivo da proteggere
  - CLG che controllano le comunicazioni (si potrebbero usare anche degli ALG, però l'overhead è notevole, magari si possono usare in punti strategici....). Questo può essere fatto rispettare ad esempio usando dei sidecar
    - Anche dei packet filter possono andare bene per far sì che le varie sezioni comunichino correttamente
  - Uso TLS quindi non posso sapere cosa due nodi stanno comunicando, ma TLS si appoggia a TCP classico quindi so con CHI stanno comunicando (tramite indirizzo e porta). Sarebbe più un problema se usassero tipo IPSec (ma in realtà questo diventa più un problema dell'architettura sottostante, i servizi IPSec non lo vedono comunque)
  - si possono usare tecniche specifiche per un certo ambiente per migliorare le tecniche di monitoring (jones:system_aware_security)
  
  Tutte queste informazioni da sole non valgono molto, la vera potenza si scopre nel momento in cui vengono **correlate** (samir:anomaly_detection). Il grossissimo problema di molte tecniche è che prevedono o la collaborazione del microservizio stesso che deve inviare tipo dei report (o inviare le comunicazioni in due posti cifrate con due chiavi diverse, al destinatario ed all'analizzatore), o prevedono di analizzare direttamente le comunicazioni intra-servizi sniffandole. Nel primo caso se un servizio è corrotto non è detto che collabori, nel secondo caso o si lasciano le comunicazione in chiaro (male) oppure si condivide tipo una master key con lo sniffer (malissimo). Molte tecniche prevedono l'uso di IA (jin:anomaly_detection, samir:anomaly_detection)
  
- Il grosso problema è che se un servizio è compromesso potrebbe diffondere informazioni corrotte all'interno del sistema, in pratica viene corrotto lo stato di un servizio dopo l'altro. Per questo motivo è importante trovare la causa reale (negli articoli "root cause" del problema)
- Dall'API gateway si possono ottenere molte informazioni, come monitoraggio delle richieste e del tipo di richieste in ingresso, tasso di arrivo delle richieste
## 4.4. Tecniche per rallentare l'esplorazione della rete dei microservizi
Materiale:

* pfleeger:security_base
* osman:threat_intelligence_microservices

Note:

  - Tarpit, interrompe la kill chain (fase reconnaissance)
  - Honeypot 
      - Alcuni KPI possono aiutare l'attaccante a rilevare gli honeypot (osman:threat_intelligence_microservices)
  - modifiche allo stato della rete (osman:threat_intelligence_microservices)
## 4.5. Threat intelligence
Materiale:

- cisco:threat_intelligence
- kaspersky:threat_intelligence
- mavroeidis:threat_intelligence
- osman:threat_intelligence_microservices
- diogenes:sybersecurity_attack_defense

Note:

* È la generazione di conoscenza basata su dati fattuali riguardo a minacce esistenti o potenziali. Questo porta vari benefici come una migliore efficienza ed una maggiore efficacia dei processi di difesa
* Sostanzialmente si occupa di analizzare tutti i dati per capire quali minacce possano presentarsi e come proteggersi. È un po' tipo la business intelligence vista in DM ma applicata alla cybersecurity
* Possiamo dividerla in:
  * Strategic threat intelligence: offre supporto alle decisioni prese in ambito di business
  * Tactic threat intelligence
  * Operational threat intelligence
* Attualmente non vi è un sistema di condivisione delle informazioni unificato che consenta di comunicare efficacemente tra gli esperti (mavroeidis:threat_intelligence)
* L'uso di tecniche di protezione aiuta il sistema a non subire danni ma allo stesso tempo rende più complesso raccogliere dati per la TI. Una soluzione consiste nel mirroring di alcuni servizi in una rete isolata per poter raccogliere info mentre il sistema reale è ancora in funzione (osman:threat_intelligence_microservices)

## 4.6. Vulnerability assessment

Materiale:

* diogenes:sybersecurity_attack_defense

Note:

- Vulnerability assessment. Controllare periodicamente le vulnerabilità del sistema o quantomeno rimanere aggiornati riguardo a nuove vulnerabilità scoperte

# 5. Reazione ad un attacco
Materiale:

- gdpr:art33
- gdpr:art34
- yarygina:low_level_exploitation
- nist:recovery
- diogenes:sybersecurity_attack_defense
- beaglesecurity:attack_response

Note:

Sistema a cinque punti per reagire ad un attacco (molte compagnie non ne hanno uno):

1. Reagire: fare qualcosa per fermare l'attacco o quantomeno per rallentarlo
2. Capire: effettuare un'analisi del danno e operazioni di threat intelligence e analisi (diogenes:sybersecurity_attack_defense)
3. Aggiustare: correggere le vulnerabilità che hanno consentito il verificarsi dell'attacco. Eventualmente investire in nuove misure (o nell'istruzione del personale). Durante le riparazioni occorre raccogliere quante più informazioni possibile per il successivo passo di threat intelligence.
4. Recovery: fare in modo che il sistema torni completamente operativo
5. Report: avvisare chi di dovere, gli utenti e le autorità. Ci sono anche delle legislazioni in merito

## 5.1. Reazione
Materiale:

- yarygina:low_level_exploitation

Note: 

- si può agire sul discovery service, se è presente, per fare in modo che i servizi corrotti non possano né effettuare richieste né essere trovati da altri servizi. Così facendo si dovrebbero rallentare i movimenti laterali. Il problema nasce nel momento in cui però è il DS ad essere corrotto
  - Ricordati del tarpit, potrebbero essere tirati su a piacere per depistare l'attaccante
- si possono usare IDS e IPS per rilevare e bloccare intrusioni. Alcuni IPS prevedono la possibilità di creare degli honeypot su richiesta, che potrebbero essere usati in concomitanza con il service discovery per cercare di capire le intenzioni dell'attaccante e quindi agire in maniera più mirata oppure per rallentarne i movimenti. IDS e IPS possono essere posizionati sia sull'API gateway che come sidecar sui vari servizi ottenendo effetti diversi
- si possono bloccare direttamente i servizi corrotti nel caso sia possibile, possono essere spenti e riaccesi (con indirizzi diversi e/o su macchine diverse) per provare ad interrompere la kill chain. Si possono usare meccanismi come gli hash per verificare inoltre che l'immagine usata per il ripristino sia integra. Se però si riavvia un servizio senza apportarvi modifiche l'attacco può di nuovo verificarsi dato che sono presenti le stesse condizioni presenti prima del riavvio. Per progettazione i microservizi dovrebbero essere stateless, per ottenere la persistenza dello stato si possono usare risorse condivise (come registri degli eventi o DB condivisi), in questo modo anche se un servizio con stato è spento gli altri possono ripartire da dove quello s'era fermato. In più se la risorsa è garantisce ACID c'è la garanzia che nessun dato venga perso anche a seguito di una riattivazione brusca. Nel caso in cui la sessione sia però corrotta devono esserci meccanismi atti a invalidarla, ma comunque dopo che è stata invalidati i servizi che dipendevano su quei dati devono gestire l'errore senza battere ciglio. Si deve anche considerare il fatto che si può istruire un balancer per inviare le richieste associate ad una stessa sessione sempre allo stesso microservizio, in questo modo lo stato viene mantenuto solamente da un microservizio e non ci sono problemi nel momento in cui è necessario capire se lo stato sia corrotto o meno (lo è), però ci sono problemi per mantenere la disponibilità del sistema (se butto giù quel servizio tutti i suoi dati vanno persi) 
- se è presente si può sfruttare l'API gateway per bloccare richieste provenienti dall'esterno e per isolare certi servizi dal ricevere richieste o comunicare dati all'esterno
- si possono usare tutte le misure più specifiche per i vari meccanismi utilizzati per mettere in campo le comunicazioni tra servizi o per ospitare i servizi (scadenza dei certificati per mTLS, scadenza dei token per l'autenticazione e/o per l'autorizzazione, modifiche temporanee alle policy per l'autorizzazione...)
- un servizio compormesso può essere usato come honeypot temporaneo per guadagnare informazioni (vedi yarygina:low_level_exploitation)
- esempio del DS per far arrivare messaggi ad un honeypot

## 5.2. Recovery
Materiale:
- mczara:dependecies
- pereira:security_mapping (in parte, un accenno)
- nist:recovery
- diogenes:sybersecurity_attack_defense

Note:
- Generale
  - Per prima cosa occorre pianificare le operazioni di recovery, non si possono improvvisare. Per fare questo bisogna avere chiare quali siano le priorità del sistema e quali parti del sistema debbano essere protette per prime (vedi [NIST](https://nvlpubs.nist.gov/nistpubs/SpecialPublications/NIST.SP.800-184.pdf)). Identificare e capire quale sia l'obiettivo di chi attacca permette di pianificare la risposta in maniera migliore
  - Allenare e verificare le capacità di recovery
  - Tutte le decisioni prese per le procedure di recovery devo basarsi su datti fattuali rilevati dalle analisi fatte dopo l'attacco (vedi threat intelligence), in modo da procedere mirati verso la riattivazione del sistema
- L'idea qua è quella di riprendere i pezzi dell'architettura e di vedere come ripristinarli
  - Comunicazioni
    - chiavi eventualmente da modificare o da condividere di nuovo
    - procedure di autenticazione (token....)
    - topologia della rete, sarebbe utile cambiare gradualmente la topologia della rte e la distribuzione dei servizi in modo che un nuovo attacco si trovi in condizioni diverse (la rete è logica)
  - Servizi
    - i servizi corrotti si possono buttare giù e rifar partire da immagini pulite (controllando con hash tipo che siano integre). Dato che non è detto che tutte le istanze di un servizio siano state corrotte può anche darsi che il servizio rimanga disponibile anche durante le operazioni di recovery del sistema. C'è tutto il discorso dello stato però. Se sono senza stato nessun problema. Se posseggono stato bisogna anche controllare che lo stato memorizzato nel coso degli eventi sia integro e non sia stato corrotto. Probabilmente in questi casi conviene sacrificare parte delle informazioni per avere una certezza maggiore riguardo alla sicurezza (sono scelte che possono essere fatte caso per caso)
  - API gateway
    - vale lo stesso discorso dei servizi, deve essere ripristinato da un'immagine pulita. Sarebbe il caso di ripristinarlo solo nel momento in cui si è ragionevolmente certi che l'intrusione sia stata debellata
  - Discovery service
    - Visto che è uno dei punti in cui può agire per rallentare una intrusione bisogna valutare bene quando ripristinarlo
    - Bisogna valutare anche che la sua cache e tutte le registrazioni dei servizi in realtà potrebbero essere state compromesse. È da valutare a seconda della criticità dei sistemi se far eseguire nuovamente un giro di registrazione ai servizi
  - Database
    - Le operazioni di recovery dei database c'entrano poco con i microservizi, accennare solo che dato che non c'è un unico database queste operazioni sono più complicate di quanto sarebbero normalmente

# 6. Conclusioni