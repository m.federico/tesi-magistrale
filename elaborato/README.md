# Elaborato

## Struttura

```bash
.
├── capitoli
├── immagini
├── bibliografia.bib
├── copertina.tex
├── frontespizio.tex
├── tesi.tex
├── Scratchapd.xml
```

All'interno della cartella capitoli si trovano i file dei capitoli, un capitolo per file.

L'elaborato in se si trova nel documento `tesi.tex`, all'interno del quale è possibile abilitare anche la compilazione di una anteprima del frontespizio che però deve essere compilato in un file diverso dato che il documento in sé è impostato con margini diversi a seconda che le pagine si trovino a sinistra o a destra nel documento finale, cosa che abbruttirebbe notevolmente la copertina che dovrebbe essere centrata. Il file `copertina.tex` contiene il codice minimo per la compilazione della copertina della tesi. Il file `frontespizio.tex` infine contiene il codice del frontespizio, è incluso sia in `copertina.tex` per ottenere la copertina finale, ma anche all'interno di `tesi.tex` per l'anteprima.

La bibliografia si trova all'interno di `bibliografia.tex`, secondo il formato `biblatex`.

`Scratchapd.xml` contiene gli oggetti usati per le immagini della tesi, può essere importato in `draw.io` per usare i template.

## Compilazione
Per ottenere il pdf dell'elaborato
```bash
latexmk --xelatex tesi.tex
```

Per ottenere il pdf della copertina
```bash
latexmk --xelatex copertina.tex
```
