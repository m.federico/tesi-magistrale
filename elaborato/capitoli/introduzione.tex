\chapter{Introduzione}
Sebbene il termine microservizio non sia completamente formalizzato e, anzi, esistano diverse definizioni, in generale con ``architettura a microservizi'' si intende una variante dell'architettura orientata ai servizi (SOA, \textit{Service Oriented Architecture}) che però prevede componenti di dimensioni minori e maggiormente disaccoppiati (\textit{loosely coupled}) chiamati microservizi~\cite{yarygina:overcoming_security}. 
Questo fatto consente anzitutto di affrontare e risolvere ogni problema per mezzo dello strumento più adatto tramite l'uso di tecnologie potenzialmente diverse, anziché di utilizzare un unico strumento per ogni problema. 
Diventa inoltre possibile sviluppare i vari componenti in parallelo, una volta che siano state definite delle interfacce stabili per la comunicazione, e scalare il numero di componenti sui singoli server con maggior facilità rispetto al caso di applicativi monolitici. 
I risultati maggiormente visibili ottenibili tramite l'uso di tale architettura sono quindi il raggiungimento di una maggior flessibilità durante sia lo sviluppo e l'evoluzione del sistema sia durante le fasi di deployment.

Nonostante questi evidenti vantaggi il disaccoppiamento dei componenti porta anche a problematiche nuove, che da un lato ricordano quelle che affliggono notoriamente i sistemi distribuiti, ma che per le caratteristiche dell'architettura vengono portate ad un altro livello. 
Un esempio tipico riguarda la gestione della sicurezza delle comunicazioni tra i singoli servizi, problematica assente nel caso classico di sistemi monolitici distribuiti, ma che è presente nelle architetture a microservizi, in quanto i vari componenti hanno la necessità materiale di comunicare con altri componenti al fine di portare a termine il proprio scopo. 
Un secondo problema invece riguarda la gestione delle responsabilità all'interno delle fasi di sviluppo e manutenzione per quanto riguarda la sicurezza, in quanto ogni componente è potenzialmente sviluppato da un diverso team. 
A complicare il tutto vi è una forte necessità di scalabilità e di automazione. 
Utilizzando questa architettura infatti ci si ritrova ad avere a che fare potenzialmente con decine, se non centinaia, di servizi differenti, fatto che rende impossibile una gestione manuale~\cite{yarygina:overcoming_security} e che richiede quindi meccanismi automatici.

Date le promesse di flessibilità e agilità nello sviluppo e nella gestione dei sistemi è naturale che l'utilizzo dell'architettura a microservizi si sia diffuso sempre di più negli ultimi anni, a partire da casi noti come Netflix, Spotify, Amazon~\cite{nginx:design, soldani:pains_gains}. 
Tuttavia, come emerge dallo studio effettuato da \citeauthor{soldani:pains_gains} in~\cite{soldani:pains_gains}, sebbene all'interno dell'industria sia emerso un certo numero di pratiche e tecniche apposite, queste generalmente non sono supportate da studi accademici. 
Questa tesi cerca di inserirsi in questo panorama variegato cercando di contribuire allo sviluppo di un ecosistema aperto di soluzioni condivise utili alla gestione della sicurezza delle informazioni ed allo stesso tempo dei sistemi.

\section{Obiettivi}
Questa tesi si pone un triplice obiettivo. 
Il primo di essi consiste nel fornire una formalizzazione dell'architettura a microservizi dal punto di vista della sicurezza informatica. Tale obiettivo viene conseguito facendo riferimento ad una prospettiva \textit{blue team} che copra, oltre all'aspetto delle comunicazioni tra i servizi, anche altri aspetti salienti dell'architettura e dei componenti necessari al suo funzionamento. 
Il secondo obiettivo consiste invece nel fornire un framework flessibile per la progettazione e l'aggiornamento di sistemi a microservizi sicuri, tenendo conto sia di aspetti prettamente legati alla sicurezza sia di aspetti invece più legati alla gestione aziendale. 
Il terzo ed ultimo obiettivo infine consiste nel fornire un riferimento utilizzabile per il mantenimento della sicurezza all'interno del sistema già attivo e per la reazione ad un attacco, descrivendo quindi sia alcuni strumenti e tecniche utilizzabili sia quali accorgimenti adottare al fine di ridurre gli eventuali danni causati dall'attacco stesso.

Quanto descritto nel resto nel resto dell'elaborato viene calato nel contesto della \textit{defense in depth} (difesa in profondità~\cite{leuprecht:castle_model}) e di un modello di attaccante (\textit{adversarial model}) secondo cui ogni messaggio così come ogni componente può essere raggiunto e manomesso. 

\section{Struttura}
L'elaborato può essere sommariamente diviso in tre sezioni distinte, una per ognuno degli obiettivi descritti precedentemente.
Il capitolo~\ref{cap:analisi} contiene l'analisi e la formalizzazione dell'architettura e dei suoi componenti principali, fornendo da un lato i concetti necessari allo sviluppo dei capitoli successivi, dall'altro un esame delle problematiche che affliggono tale architettura intesa nel suo senso generale, ovvero non calata all'interno di un preciso contesto di \textit{deployment}, e delle relative soluzioni.

All'interno del capitolo~\ref{cap:progettazione} viene proposto un framework per la progettazione di un sistema a microservizi sicuro assieme ad alcuni concetti più generali, non necessariamente associati al mondo dei microservizi, ma che comunque possono incrementare il livello di sicurezza del sistema progettato e che dunque sarebbe bene tenere a mente.

All'interno dei capitoli~\ref{cap:mantenimento} e~\ref{cap:reazione} infine vengono descritte pratiche e metodi utili rispettivamente per mantenere un adeguato livello di sicurezza all'interno di un sistema a microservizi e per reagire ad un attacco in corso oppure portato a termine.


