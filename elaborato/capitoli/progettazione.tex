\chapter{Progettazione di un sistema sicuro}\label{cap:progettazione}
Dopo aver visto nel precedente capitolo le caratteristiche in termini di sicurezza dell'architettura a microservizi, in questo capitolo ci concentriamo sulle tecniche che possono essere utilizzate per la progettazione di un sistema sicuro.
Le motivazioni che spingono ad utilizzare tecniche standard o condivise per la progettazione di un sistema sono molteplici, ma possono essere riassunte con la necessità di fattorizzare lo sforzo necessario alla progettazione, ovvero ``non reinventare la ruota'', e con la consapevolezza che se una tecnica è stata vagliata da più persone diverse molto probabilmente questa ha meno difetti rispetto ad una sviluppata artigianalmente. 
Lo schema proposto nelle sezioni successive è un'estensione e rielaborazione di quanto descritto da \citeauthor{carter:preliminary_methodology} in~\cite{carter:preliminary_methodology} relativamente alla progettazione di sistemi cyber-fisici (\textit{cyber-physical systems}), integrata con alcuni concetti descritti da \citeauthor{galanti:tesi} in~\cite{galanti:tesi} fino a formare lo scheletro di un nuovo framework per la progettazione di sistemi sicuri.

\section{Tecniche e metodologia}
Il framework coinvolge tre team (vedi tabella~\ref{tab:tre_team}) all'interno di un processo ciclico la cui singola iterazione può essere vista come il susseguirsi di tre fasi. 
Il primo team, codificato con il nome di \textit{yellow team} per seguire il parallelo con lo schema descritto in~\cite{carter:preliminary_methodology}, comprende individui con diverse esperienze e competenze che ricadono al di sotto dell'ombrello dell'ingegneria del software o delle \textit{operations}. Questo è il team responsabile dello sviluppo vero e proprio del sistema, dell'aggiornamento dei componenti e dei requisiti così come della gestione del processo nel suo complesso. 
La seconda squadra è il \textit{blue team}, composta da individui abili nella rilevazione e gestione dei rischi. Qualità utili per i membri facenti parte di questo team sono una certa abilità nella gestione delle operations e l'abilità di fornire un resoconto basato su priorità. 
Infine il \textit{red team} comprende individui specializzati nel rilevamento di potenziali attacchi e nella valutazione dell'efficacia delle contromisure prese. 
È importante inoltre che i tre gruppi non operino in maniera indipendente, quanto più in maniera coesa in modo da condividere conoscenze e problemi al fine di trovare rapidamente una soluzione comune che sia fattibile dal punto di vista realizzativo all'interno del sistema ed al contempo robusta dal punto di vista della sicurezza.

\begin{longtable}[c]{@{}cc@{}}
    \toprule
    Team     & Mansioni                                                                                 \\* \midrule
    \endfirsthead
    \endhead
    \bottomrule
    \endfoot
    \endlastfoot

    Yellow Team &   \begin{tabular}[c]{@{}c@{}}
                        Progettazione del sistema,\\ 
                        applicazione del framework,\\
                        aggiornamento della documentazione
                    \end{tabular}\\
    \hline
    Blue Team   &   \begin{tabular}[c]{@{}c@{}}
                        Studio della descrizione del sistema,\\ 
                        valutazione della robustezza,\\ 
                        individuazione soluzioni ai problemi di sicurezza
                    \end{tabular} \\
    \hline
    Red Team &      \begin{tabular}[c]{@{}c@{}}
                        Identificazione delle vulnerabilità,\\ 
                        analisi statiche
                    \end{tabular} \\* \bottomrule
    
    \caption{Ruoli dei tre team}\label{tab:tre_team}
\end{longtable}

Possiamo passare ora a descrivere il framework in sé facendo riferimento allo schema in figura~\ref{fig:progettazione_framework}, all'interno del quale si possono individuare tre passi sul braccio principale e tre sul braccio in retroazione. Partendo da una descrizione iniziale del sistema, proveniente da una iterazione precedente oppure creata \textit{ad hoc} dallo yellow team durante la prima iterazione, la prima fase prevede una valutazione da parte del blue team e del red team al fine di rilevare, se presenti, i problemi di sicurezza più evidenti. È bene che le analisi effettuate durante questa fase siano varie e diversificate, in modo da cogliere il maggior numero possibile di eventi e dettagli possibile. Ad esempio è possibile modellare il sistema e le interazioni con esso tramite l'utilizzo di \textit{misuse case}, concettualmente simili agli \textit{use case} usati nella progettazione software ma focalizzati su come un utente potrebbe approfittare delle caratteristiche di un sistema al fine di abusarne, oppure si possono usare tecniche più avanzate come quelle delineate da \citeauthor{sun:automated_analysis} in~\cite{sun:automated_analysis}, i quali propongono l'utilizzo di strumenti basati sull'analisi semantica dei requisiti al fine costruire un modello logico del funzionamento del sistema, modello su cui poi è possibile operare delle query specifiche per attacchi noti. Un ulteriore strumento utile durante questa fase è quello descritto da \citeauthor{atighetchi:requirement_analysis} in~\cite{atighetchi:requirement_analysis}, sebbene attualmente sia solamente una proposta, dove gli autori propongono l'utilizzo di strumenti automatici per la rilevazione di conflitti all'interno delle specifiche dei requisiti, consentendo quindi di identificare situazioni incoerenti che necessariamente portano a falle nella sicurezza oppure a requisiti non soddisfatti. 
Una volta completate le valutazioni dei due team, si passa all'elaborazione dei risultati ottenuti. Durante questa fase è importante capire se i risultati ottenuti dalle analisi combinate dei due team, le priorità dell'organizzazione ed i requisiti siano compatibili. 

\begin{figure}[h!tp]
    \centering
        \input{immagini/progettazione_framework.eps_tex}
        \caption{Framework per la progettazione di un sistema sicuro. Le etichette applicate ai rettangoli tratteggiati indicano il parallelo con lo schema presentato da \citeauthor{galanti:tesi} in~\cite{galanti:tesi}}
        \label{fig:progettazione_framework}
\end{figure}

Terminata la fase di valutazione occorre decidere se procedere con una nuova iterazione oppure se le caratteristiche trovate siano sufficientemente valide. 
Nel caso in cui i risultati ottenuti non siano adeguati, ovvero non rispettino i requisiti, diviene necessario aggiornare le difese messe a protezione del sistema partendo dai dati e dalle valutazioni appena effettuate in modo da soddisfare i requisiti rimasti scoperti. A seguito di ciò occorre tracciare quanto eseguito in modo da poterne tenerne conto nelle iterazioni successive. 
Nel caso invece in cui la soluzione non sia accettabile, ovvero rispetti i requisiti ma non le priorità, diventa necessario rielaborarla e sottoporla nuovamente a tutte le verifiche descritte nel precedente capoverso.
Se invece la soluzione è sia adeguata che accettabile il ciclo termina producendo come \textit{deliverable} la soluzione appena trovata ed un documento che attesti le caratteristiche di sicurezza del sistema. Questi documenti possono essere poi riutilizzati per riapplicare il framework qualora fosse necessario, ad esempio a seguito di modifiche ai requisiti, alle priorità oppure al sistema stesso. 

\medskip
La procedura appena descritta ben si adatta ad essere utilizzata durante tutto il ciclo di vita di un progetto e di un sistema, tuttavia gli effetti sul sistema e sui costi variano a seconda del momento specifico. Come evidenziato da \citeauthor{saravi:cost} in~\cite{saravi:cost} l'efficacia di una modifica al progetto decresce all'avanzare dello stato del progetto stesso, mentre il costo associato ad ogni modifica cresce allo scorrere del tempo (vedi figura~\ref{fig:progettazione_impatto_costi}). Per questo motivo è bene utilizzarla in particolare durante le prime fasi di progettazione.
\begin{figure}[h!tp]
    \centering
        \input{immagini/progettazione_impatto_costi_decisioni.eps_tex}
        \caption{Confronto fra (1)  l'efficacia di una modifica al progetto e (2) il costo di modifiche al progetto}\label{fig:progettazione_impatto_costi}
\end{figure}

Il framework infine è di carattere generale in modo da poter essere adattato a varie situazioni ed essere integrato con le tecniche di sviluppo Agile spesso usate per la progettazione di sistemi a microservizi, tuttavia nella sua forma di base offre solamente un'organizzazione logica del lavoro. È possibile renderlo più efficace e specifico tenendo a mente alcuni principi complementari, alcuni dei quali emergono direttamente dall'utilizzo dei microservizi stessi~\cite{yarygina:overcoming_security}. 

\section{Identificazione delle priorità}\label{sec:priorita}
Il processo appena descritto prevede la necessità di individuare quali siano le priorità all'interno del sistema e del contesto in cui questo viene progettato al fine di concentrare energie e risorse per la difesa di queste. In un mondo ideale non esisterebbero priorità e tutti gli aspetti di un sistema riceverebbero il giusto quantitativo di attenzione e cura, tuttavia nel momento in cui un qualsiasi processo viene calato in un contesto reale si palesa l'irrealizzabilità di questa idea a causa di vari fattori, primi fra i quali tempo e risorse. Diventa quindi importante capire quali siano le priorità all'interno di un sistema o di un progetto, come queste possano essere scomposte in obiettivi semplici e come i singoli obiettivi dipendano l'uno dall'altra.
Proponiamo dunque un possibile processo che consenta di rispondere a queste domande, o quantomeno che indichi una sequenza precisa per lo svolgimento delle varie fasi (vedi figura~\ref{fig:progettazione_individuazione_obiettivi}).
\begin{figure}[h!tp]
    \centering
        \input{immagini/progettazione_individualzione_obiettivi.eps_tex}
        \caption{Processo per l'individuazione degli obiettivi da proteggere e delle relative priorità}
        \label{fig:progettazione_individuazione_obiettivi}
\end{figure}

La prima fase consiste nella generazione del grafo delle dipendenze (vedi figura~\ref{fig:progettazione_individuazione_obiettivi_grafo_1}) tra i vari componenti $c_j$ a partire dalla descrizione del sistema. Questo grafo mostra in forma visiva quali siano le relazioni e le dipendenze tra i vari 
\begin{figure}[h!tp]
    \centering
        \input{immagini/progettazione_individualzione_obiettivi_grafo_1.eps_tex}
        \caption{Grafo delle dipendenze}
        \label{fig:progettazione_individuazione_obiettivi_grafo_1}
\end{figure}
componenti e può quindi essere usato per capire come il malfunzionamento di un componente possa riverberarsi anche sugli altri componenti del sistema e quindi quali effetti possa avere. Di conseguenza ognuno dei nodi del grafo può essere visto come un obiettivo da proteggere e di cui occuparsi. Possiamo trovare due tipi di obiettivi.
Gli obiettivi strutturali non sono importanti in sé in quanto non forniscono nessun servizio agli utenti, tuttavia essi risultano importanti all'interno di un'ottica globale in quanto forniscono funzionalità di base necessarie al funzionamento di altri obiettivi.
Gli obiettivi strategici invece sono importanti in sé in quanto forniscono direttamente un servizio agli utenti del sistema e dunque hanno valore nell'ottica dell'erogazione di tale servizio. Essi possono anche avere caratteristiche strutturali e fornire funzionalità di base che possono essere utilizzate da altri obiettivi.
Parallelamente occorre individuare le funzionalità di alto livello del sistema e assegnare ad ognuna di esse un grado di importanza sulla base delle politiche e gli scopi dell'organizzazione che gestisce il sistema.

Una volta completati questi due passi è possibile aggregare tutte le informazioni al fine di assegnare ad ogni obiettivo la corrispondente priorità (vedi figura~\ref{fig:progettazione_individuazione_obiettivi_grafo_2}). Viene inserito all'interno del grafo un nodo corrispondente ad ognuna delle funzionalità $f_i$ ed un arco per ogni coppia $(f_i,\ c_j)$ se il componente $c_j$ supporta direttamente la funzionalità $f_i$. 

\begin{figure}[h!tp]
    \centering
        \input{immagini/progettazione_individualzione_obiettivi_grafo_2.eps_tex}
        \caption{Grafo delle dipendenze aggiornato}
        \label{fig:progettazione_individuazione_obiettivi_grafo_2}
\end{figure}

A partire da questa forma finale del grafo è quindi possibile individuare le priorità finali, tenendo conto sia dell'importanza delle funzionalità $f_i$ sia del numero di archi, entranti ed uscenti, dei componenti $c_j$.

